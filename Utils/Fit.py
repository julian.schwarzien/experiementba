import torch

import time

import numpy as np

def train_loop(model, opt, loss_fn, device, dataloader, known_seq_len):
    
    model.train()
    total_loss = 0
    
    for batch in dataloader:
        
        X, y_input, y_expected = batch
        
        X = X.to(device)
        y_input = y_input.to(device)
        y_expected = y_expected.to(device)
        
        # transmute to [batch size, sequence len, num. features]
        X = X.permute(0,2,1)
        y_input = y_input.permute(0,2,1)
        y_expected = y_expected.permute(0,2,1)

        seq_len = X.shape[1]
        
        y_input = y_input[:, 0:known_seq_len, :]
        
        for i in range(seq_len - known_seq_len):
            
            X_temp = X[:, i:(known_seq_len + i), :]
            y_input_temp = y_input[:, i:(known_seq_len + i), :]
            
            
            pred = model(X_temp,
                         y_input_temp,
                        )
            
            y_out = pred[:, -1:, :]
            
            for j in range(y_out.shape[0]):
                for k in range (y_out.shape[2]):
                    if y_out[j][0][k] > 1.0: y_out[j][0][k] = 1.0
                    if y_out[j][0][k] < 0.0: y_out[j][0][k] = 0.0
                    
            y_input = torch.cat((y_input, y_out),1)
        
        
        loss = loss_fn(y_input.contiguous(), y_expected.contiguous())
        
        opt.zero_grad()
        loss.backward()
        opt.step()
    
        total_loss += loss.detach().item()
        
    return total_loss / seq_len

def val_loop(model, loss_fn, device, dataloader, known_seq_len):
    
    model.eval()
    total_loss = 0
    
    with torch.no_grad():
        for batch in dataloader:

            X, y_input, y_expected = batch

            X = X.to(device)
            y_input = y_input.to(device)
            y_expected = y_expected.to(device)

            # transmute to [batch size, sequence len, num. features]
            X = X.permute(0,2,1)
            y_input = y_input.permute(0,2,1)
            y_expected = y_expected.permute(0,2,1)
            
            seq_len = X.shape[1]
            
            y_input = y_input[:, 0:known_seq_len, :]
            
            for i in range(seq_len - known_seq_len):

                X_temp = X[:, i:(known_seq_len + i), :]
                y_input_temp = y_input[:, i:(known_seq_len + i), :]
                
                pred = model(X_temp,
                            y_input_temp,
                        )

                y_out = pred[:, -1:, :]
                
                for j in range(y_out.shape[0]):
                    for k in range (y_out.shape[2]):
                        if y_out[j][0][k] > 1.0: y_out[j][0][k] = 1.0
                        if y_out[j][0][k] < 0.0: y_out[j][0][k] = 0.0
            
                y_input = torch.cat((y_input, y_out),1)
                
        loss = loss_fn(y_input.contiguous(), y_expected.contiguous())
                
        total_loss += loss.detach().item()
        
    return total_loss / seq_len


def fit(model, opt, loss_fn, device, train_dataloader, val_dataloader, epochs, known_seq_len, i):    
    
    train_loss_list = []
    validation_loss_list = []
    
    #print("Training and validating model")
    
    start_time = time.time()
    
    best_val = np.inf
    best_epoch = -1
    best_time = start_time
    
    for epoch in range(epochs):

        train_loss = train_loop(model, opt, loss_fn, device, train_dataloader, known_seq_len)
        train_loss_list += [train_loss]
        
        validation_loss = val_loop(model, loss_fn, device, val_dataloader, known_seq_len)
        validation_loss_list += [validation_loss]        
        
        if validation_loss < best_val:
            best_time = time.time()
            best_val = validation_loss
            best_epoch = epoch
            torch.save(model, f"models/cFormer_{i}")
        
        #print("-"*25, f"Epoch {epoch +1}","-"*25)
        #print(f"Training loss: {train_loss:.8f}")
        #print(f"Validation loss: {validation_loss:.8f}")
        #print()
            
    end_time = time.time()
    
    torch.save(model, f"models/cFormer_{i}_fin")
    
    train_time = end_time - start_time

    #print(f"best epoch was: {best_epoch} with val loss: {best_val: .8f} after: {round(best_time - start_time, 0)} seconds")
    #print(f"Completed in {train_time} seconds")
    
    return train_loss_list, validation_loss_list, train_time