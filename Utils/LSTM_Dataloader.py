from torch.utils.data import DataLoader

import numpy as np

def load_data(data, seq_len, batch_len):
    data_out = []
    for i in range(len(data) - seq_len ):
        X = np.array([data['X_set.value'].iloc[i + 1: i + seq_len + 1 ],
                     ])
        y_in = np.array([data['F'].iloc[i],
                          data['P.value_norm'].iloc[i]
                         ])
        y_exp = np.array([data['F'].iloc[i + 1 : i + seq_len + 1 ],
                          data['P.value_norm'].iloc[i + 1 : i + seq_len + 1]
                         ])
        
        data_out.append([X, y_in, y_exp])
        
    return DataLoader(data_out, batch_size=None, shuffle=True)