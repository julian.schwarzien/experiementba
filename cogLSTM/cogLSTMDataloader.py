from torch.utils.data import DataLoader

import numpy as np

def load_data(data, seq_len, batch_len):
    data_out = []
    for i in range(len(data) - seq_len ):
        X = np.array([data['X_set.value'].iloc[i + 1: i + seq_len + 1 ],
                     ])
        
        y_in = np.array([data['P.value'].iloc[i],
                         data['Qh.value'].iloc[i],
                         data['H.value'].iloc[i],
                         data['pm.T_cw'].iloc[i],
                         data['pm.T_eng'].iloc[i],
                        ])
        
        y_exp = np.array([data['P.value'].iloc[i + 1 : i + seq_len + 1],
                          data['Qh.value'].iloc[i + 1 : i + seq_len + 1],
                          data['H.value'].iloc[i + 1 : i + seq_len + 1],
                          data['pm.T_cw'].iloc[i + 1 : i + seq_len + 1],
                          data['pm.T_eng'].iloc[i + 1 : i + seq_len + 1],
                         ])
        
        data_out.append([X, y_in, y_exp])
        
    return DataLoader(data_out, batch_size=None, shuffle=True)