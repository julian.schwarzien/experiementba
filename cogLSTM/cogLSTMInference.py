import torch

import time

import numpy as np

def infer(model, data, num_layers, device):
    
    model.eval()
    
    with torch.no_grad():
        
        start = time.time()
        
        data_len = len(data['X_set.value'])
        
        #get X, y_input
        X = np.array([data['X_set.value'].iloc[0:],
                     ])
        
        y = np.array([data['P.value'].iloc[0:],
                      data['Qh.value'].iloc[0:],
                      data['H.value'].iloc[0:],
                      data['pm.T_cw'].iloc[0:],
                      data['pm.T_eng'].iloc[0:]
                     ])
        
        X = torch.tensor(X)
        y = torch.tensor(y)
        
        X = X.to(device)
        y = y.to(device)
        
        y = y.permute(1,0)
        
        # transmute to [batch size, sequence len, num. features]
        X = X.reshape([-1, 1])
        y = y.reshape([-1, 5])

        hn = np.array([data['P.value'].iloc[0],
                       data['Qh.value'].iloc[0],
                       data['H.value'].iloc[0],
                       data['pm.T_cw'].iloc[0],
                       data['pm.T_eng'].iloc[0]
                     ])
        hn = torch.tensor(hn).to(device)

        hn = torch.reshape(hn, (1,5))
        hn = hn.repeat(num_layers,1)
        
        cn = torch.zeros(num_layers,5).to(device)

        pred, (hn, cn) = model(X,(hn,cn))
            
        print(f"Inf finished in {time.time() - start} sek")
        
    return pred, time.time() - start