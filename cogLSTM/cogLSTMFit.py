import torch

import time

import numpy as np

def train_loop(model, opt, loss_fn, dataloader, num_layers, device):

    model.train()
    total_loss = 0

    for element in dataloader:

        X, y_input, y_expected = element

        X = X.to(device)
        y_input = y_input.to(device)
        y_expected = y_expected.to(device)
        
        # transmute to [batch size, sequence len, num. features]
        X = X.permute(1,0)
        y_expected = y_expected.permute(1,0)

        hn = y_input
        
        hn = torch.reshape(hn, (1,5))
        hn = hn.repeat(num_layers,1)
        
        cn = torch.zeros(num_layers,5).to(device)
        
        pred, (hn, cn) = model(X, (hn, cn))
        
        loss = loss_fn(pred, y_expected)

        opt.zero_grad()
        loss.backward()
        opt.step()

        total_loss += loss.detach().item()

    return total_loss / len(dataloader)

def validation_loop(model, loss_fn, dataloader, num_layers, device):
    
    model.eval()
    total_loss = 0
    
    with torch.no_grad():
        for element in dataloader:

            X, y_input, y_expected = element

            X = X.to(device)
            y_input = y_input.to(device)
            y_expected = y_expected.to(device)

            # transmute to [batch size, sequence len, num. features]
            X = X.permute(1,0)
            y_expected = y_expected.permute(1,0)

            hn = y_input
        
            hn = torch.reshape(hn, (1,5))
            hn = hn.repeat(num_layers,1)
        
            cn = torch.zeros(num_layers,5).to(device)

            pred, (hn, cn) = model(X, (hn, cn))
            
            
            loss = loss_fn(pred, y_expected)

            total_loss += loss.detach().item()
        
    return total_loss / len(dataloader)

def fit(model, opt, loss_fn, train_dataloader, val_dataloader, epochs, i, num_layers, device):
    
    train_loss_list = []
    validation_loss_list = []
    
    local_time = time.strftime("%H:%M", time.localtime())
    
    print("Training and validating model")
    print(local_time)
    
    start_time = time.time()
    
    best_val = np.inf
    best_epo = -1
    best_time = start_time
    
    for epoch in range(epochs):

        train_loss = train_loop(model, opt, loss_fn, train_dataloader, num_layers, device)
        train_loss_list += [train_loss]
        
        validation_loss = validation_loop(model, loss_fn, val_dataloader, num_layers, device)
        validation_loss_list += [validation_loss]        
        
        if validation_loss < best_val:
            best_time = time.time()
            best_val = validation_loss
            best_epo = epoch
            torch.save(model, f"models/cogLSTM_{i}")
        
        """
        print("-"*25, f"Epoch {epoch +1}","-"*25)
        print(f"Training loss: {train_loss:.8f}")
        print(f"Validation loss: {validation_loss:.8f}")
        print()
        """

    end_time = time.time()
    
    torch.save(model, f"models/cogLSTM_{i}_fin")
    """
    print(f"best epoch was: {best_epo} with val loss: {best_val: .8f} after: {round(best_time - start_time, 0)} seconds")
    print(f"Completed in {round(end_time - start_time, 0)} seconds")
    """

    train_time = end_time - start_time

    return train_loss_list, validation_loss_list, train_time