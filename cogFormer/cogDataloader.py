from torch.utils.data import DataLoader

import numpy as np

# Sample all possible data sets of length
def load_data(data, seq_len, batch_len):

    data_out = []
    
    #create all posible sequences of seq_len
    for i in range(len(data) - seq_len):
        
        X = np.array([data['X_set.value'].iloc[(i + 1) : (i + 1 + seq_len)]
                     ])
        
        #y_input is leftshifted version of y_expected
        y_input = np.array([data['P.value'].iloc[i : (i + seq_len)],
                            data['Qh.value'].iloc[i : (i + seq_len)],
                            data['H.value'].iloc[i : (i + seq_len)],
                            data['pm.T_cw'].iloc[i : (i + seq_len)],
                            data['pm.T_eng'].iloc[i : (i + seq_len)],
                           ])
        
        y_expected = np.array([data['P.value'].iloc[(i + 1) : (i + seq_len + 1)],
                               data['Qh.value'].iloc[(i + 1) : (i + seq_len + 1)],
                               data['H.value'].iloc[(i + 1) : (i + seq_len + 1)],
                               data['pm.T_cw'].iloc[(i + 1) : (i + seq_len + 1)],
                               data['pm.T_eng'].iloc[(i + 1) : (i + seq_len + 1)],
                              ])

        data_out.append([X, y_input, y_expected])

    return DataLoader(data_out, batch_size=batch_len, shuffle=True)