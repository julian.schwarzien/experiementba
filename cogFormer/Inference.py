import torch

import time

import numpy as np

def infer(model, data, device, known_seq_len):
    
    model.eval()
    
    with torch.no_grad():

        start = time.time()
        
        data_len = len(data['X_set.value'])

        #get X, y_input
        X = np.array([data['X_set.value'].iloc[0:]
                     ])

        y  = np.array([data['P.value'].iloc[0:known_seq_len],
                       data['Qh.value'].iloc[0:known_seq_len],
                       data['H.value'].iloc[0:known_seq_len],
                       data['pm.T_cw'].iloc[0:known_seq_len],
                       data['pm.T_cw'].iloc[0:known_seq_len]
                      ])

        X = torch.tensor(X)
        y = torch.tensor(y)
        
        X = X.to(device)
        y = y.to(device)
        
        y = y.permute(1, 0)
        
        X = X.reshape([1, -1, 1])
        y = y.reshape([1, -1, 5])

        for i in range(data_len - known_seq_len):

            #shape is  [batch size, num. features, sequence len]
            X_temp = X[:, i:(known_seq_len + i), :]
            y_temp = y[:, i:(known_seq_len + i), :]
            
            pred = model(X_temp,
                        y_temp,
                        )
            
            y_out = pred[:, -1:, :]
            
            for j in range(y_out.shape[0]):
                for k in range (y_out.shape[2]):
                    if y_out[j][0][k] > 1.0: y_out[j][0][k] = 1.0
                    if y_out[j][0][k] < 0.0: y_out[j][0][k] = 0.0

            y = torch.cat((y, y_out),1)

    #y = y[len(y)-1000:]

    return y, time.time() - start 