from Utils.Transformer import TimeSeriesTransformer
from cogFormer.cogDataloader import load_data
from cogFormer.Inference import infer
from cogFormer.Fit import fit

import pandas as pd

from tqdm import tqdm

import torch
import torch.nn as nn
import torch.optim as optim
import torch.multiprocessing as mp
from torch.multiprocessing import Process, Queue

from scipy.stats import qmc

from torchmetrics import R2Score

import numpy as np

#sample, i, epochs, train_dataloader, val_dataloader, df_val, r2, device, result_lock, loss_lock

class Worker(Process):
    def __init__(self, id, device, queue):
        self.id = id
        self.device = device
        print(f'Worker {self.id} started on device {self.device}')
        self.queue = queue
        # do some initialization here
        super(Worker, self).__init__()

    def run(self):
         for data in iter(self.queue.get, None):
            if data is not None:
                (sample, i, epochs, seq_len, train_dataloader, val_dataloader, df_val, r2, device, result_lock, loss_lock) = data
                print(f"Computing on Worker {self.id} on device {self.device}:", sample, i, epochs, train_dataloader, val_dataloader, df_val, r2, device, result_lock, loss_lock, flush=True)

    
                torch.set_default_dtype(torch.float64)

                #data parameters
                input_size = 1
                num_predicted_features = 5

                #model parameters

                #learning rate
                #set between 10^-2 and 10^-7
                lr = sample[i][0] * (-5) - 2
                lr = lr.round()
                lr = 10**lr

                #n_heads
                #set between 4 and 12
                n_heads = sample[i][1] * 8 + 4
                n_heads = n_heads.round()
                n_heads = n_heads.astype("int")

                #dim_val
                #set between 48 and 80
                #used as a multyplier for n_head, resulting values are between 192 and 960
                dim_val = sample[i][2] * 32 + 48
                dim_val = dim_val.round()
                dim_val = dim_val.astype("int")
                dim_val = dim_val * n_heads

                #n_encoder layers
                #set between 1 and 6
                n_encoder_layers = sample[i][3] * 5 + 1
                n_encoder_layers = n_encoder_layers.round()
                n_encoder_layers = n_encoder_layers.astype("int")

                #n_decoder layers
                #set between 1 and 6
                n_decoder_layers = sample[i][4] * 5 + 1
                n_decoder_layers = n_decoder_layers.round()
                n_decoder_layers = n_decoder_layers.astype("int")

                #known_seq_len
                #set between 1 and 16
                known_seq_len = sample[i][5] * 15 + 1
                known_seq_len = known_seq_len.round()
                known_seq_len = known_seq_len.astype("int")
                
                #create Model
                model = TimeSeriesTransformer(
                    input_size=input_size,
                    num_predicted_features=num_predicted_features,
                    n_decoder_layers=n_decoder_layers,
                    n_encoder_layers=n_encoder_layers,
                    batch_first=True,
                    ).to(device)
                opt = optim.Adam(model.parameters(), lr)
                loss_fn = nn.MSELoss()

                # train model
                train_loss_list, val_loss_list, train_time = fit(model, opt, loss_fn, device, train_dataloader, val_dataloader, epochs, known_seq_len, i)
                #model = torch.load("models/cogFormer_0")
                #train_loss_list = []
                #val_loss_list = []
                #train_time = 1.0


                #run inference
                inf, inf_time = infer(model, df_val, device, known_seq_len)

                #calculate r2 score of the result
                r2score, r2_target = r2

                r2_result = r2score(inf[0].cpu(), r2_target)

                #write results to file
                #i, r2 score, inference duration, learing rate, n_heads, dim_val, n_encoder_layers, n_decoder_layers, known sequence length
                with result_lock:
                    results = open("cogFormerresults.csv", "a")
                    results.write(f"{i}, {r2_result.item()}, {train_time}, {inf_time}, {lr}, {n_heads}, {dim_val}, {n_encoder_layers}, {n_decoder_layers}, {known_seq_len},\n")
                    results.close()

                #write loss lists to file
                #i, train_loss_list
                # , val_loss_list
                with loss_lock:
                    loss = open("cogFormerloss.csv", "a")
                    loss.write(f"{i}, {train_loss_list},\n , {val_loss_list},\n")
                    loss.close()

                print(f"{i} finished")

            else:
                return True
            
        #return True

if __name__ == '__main__':
    #parameters for sampler
    #actual number of samples is 2**num_samples
    num_samples = 5     #actually 2**5 = 64 samples
    epochs = 200

    #each process needs up to 4gb of vram, set number accordingly
    #if ram is exceeded a lot of samples will fail
    max_processes = 2
    
    #seclect cuda if availabe
    device = "cuda" if torch.cuda.is_available() else "cpu"

    #read prepared data from csv
    #todo: adjust
    df = pd.read_csv('cog_data.csv')

    #get taining data from df and reset index
    df_train = df.iloc[0:1500]
    df_train = df_train.set_index(np.arange(0, len(df_train)))

    #get validation data from df and reset index
    df_val = df.iloc[2000:3000]
    df_val = df_val.set_index(np.arange(0, len(df_val)))

    #open file to write results
    #overwrites current results
    results = open("cogFormerresults.csv", "w")
    results.write(" , r2 score,training duration, inference duration, learning rate, n_heads, dim_val, n_encoder_layers, n_decoder_layers, known sequence length,\n")
    results.close()

    loss = open("cogFormerloss.csv", "w")
    loss.close()

    #set up r2 norm
    r2_target = torch.Tensor(np.array([
        df_val['P.value'].iloc[0:],
        df_val['Qh.value'].iloc[0:],
        df_val['H.value'].iloc[0:],
        df_val['pm.T_cw'].iloc[0:],
        df_val['pm.T_cw'].iloc[0:]])).cpu()

    r2_target = r2_target.permute(1, 0)
    r2score = R2Score(num_outputs=5, multioutput='uniform_average')

    r2 = (r2score, r2_target)

    #set up sampler
    sampler = qmc.Sobol(d=6, seed=42)
    samples = sampler.random_base2(m=num_samples)

    #parameters for dataloader
    batch_len = 16
    seq_len = 32

    #generate training and validation data
    train_dataloader = load_data(df_train, seq_len, batch_len)
    val_dataloader = load_data(df_val, seq_len, batch_len)

    map_list = []

    #cuda only works with the spawn method
    mp.set_start_method('spawn')

    #create locks for output files
    manager = mp.Manager()
    result_lock = manager.Lock()
    loss_lock = manager.Lock()

    #create iterable list of inputs
    for i in range(len(samples)):
        map_list.append((samples, i, epochs, train_dataloader, val_dataloader, df_val, r2, device, result_lock, loss_lock))

    request_queue = Queue()
    processes = []
    for i in range(max_processes):
        processes.append(Worker(i, "cuda:"+str(i//2), request_queue))
    for i in range(2**num_samples):
        request_queue.put((samples, i, epochs, seq_len, train_dataloader, val_dataloader, df_val, r2, device, result_lock, loss_lock))
    # Sentinel objects to allow clean shutdown: 1 per worker.
    for i in range(max_processes):
        request_queue.put(None)

    for p in processes:
        p.start()

    for p in processes:
        p.join()

    #create the pool
    #pool = mp.Pool(processes = max_processes)

    #result = pool.starmap_async(worker, (map_list))

    #result = worker(samples, i, epochs, train_dataloader, val_dataloader, df_val, r2, device, result_lock, loss_lock)

    #pool.close()

    #wait for all tasks in the pool to complete
    #pool.join()

    #legacy custom pool
    """
    #free memory
    torch.cuda.empty_cache()
    gc.collect()

    waiting_processes = []
    running_processes = []

    #create one process per sample
    for i in range(2**num_samples):
        p = mp.Process( target=worker, args=(locks, sample, i, epochs, train_dataloader, val_dataloader, df_val, r2, device))
        waiting_processes.append(p)

    #execute all waiting processes
    with tqdm(total=2**num_samples) as pbar:
        while (len(waiting_processes) + len(running_processes)) > 0:

            #if less then max_processes processes are running and processes are waiting start new process
            if(len(running_processes) < max_processes) & (len(waiting_processes) > 0):
                p = waiting_processes.pop(0)
                running_processes.append(p)
                p.start()

            #check if process marked as running is finished
            for p in running_processes:
                if p.is_alive() == False:
                    running_processes.remove(p)
                    pbar.update(1)
    """
